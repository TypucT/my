
alias ll='ls -l$@'
alias llh='ls -lt$@|head'
alias syncme='echo -n "Copying files FROM repository ..." ; rsync -au milen@goldfish:~milen/backup/projects ~milen; echo " DONE"'
alias syncup='echo -n "Copying files TO repository ..." ; rsync -au ~milen/projects milen@goldfish:~milen/backup; echo " DONE"'
alias vi-http='sudo vi /etc/httpd/conf/httpd.conf'
alias vi-php='sudo vi /etc/php/php.ini'
alias vi-mysql='sudo vi /etc/mysql/my.cnf'
alias tmux-svm='~/wiki/tmux/svm-tmux-env.sh'
alias tmux-svm-join='tmux a -t svm'

alias giti='cat ~milen/wiki/git/workflow/fix-issue.txt'
alias gitf='cat ~milen/wiki/git/workflow/new-feature.txt'
alias gitr='cat ~milen/wiki/git/workflow/new-release.txt'

alias restart-docky='killall docky 1>&2 2>/dev/null; nohup docky 1>&2 2>/dev/null &'

alias sm-pr='cd /srv/http/dev/salon-manager/; vi'
alias cd-sm='cd /srv/http/dev/salon-manager/'
alias vi-bl='vi /srv/http/dev/salon-manager/backlog'

alias cd-gr='cd /srv/http/dev/grafik/'

# Working with VirtualBox
alias startdb='VBoxManage startvm "FreeBSD" --type headless'
alias stopdb='VBoxManage controlvm "FreeBSD" poweroff'
alias vms='VBoxManage list runningvms'
alias allvms='VBoxManage list vms'

# Some command line tools
# Get external IP address:
alias extip='host `curl -s inet-ip.info`'

# Change directory using fuzzi finder
alias cdf='cd `dirname $(fzf)`'
alias vif='vi $(fzf)`

export sites=/srv/http/dev
export EDITOR=vim
export VISUAL=vim
