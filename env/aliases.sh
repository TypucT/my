set -o vi

alias ll='ls -l$@'
alias llh='ls -lt$@|head'
alias syncme='echo -n "Copying files FROM repository ..." ; rsync -au milen@goldfish:~milen/backup/projects ~milen; echo " DONE"'
alias syncup='echo -n "Copying files TO repository ..." ; rsync -au ~milen/projects milen@goldfish:~milen/backup; echo " DONE"'
alias vi-http='sudo vi /etc/httpd/conf/httpd.conf'
alias vi-php='sudo vi /etc/php/php.ini'
alias vi-mysql='sudo vi /etc/mysql/my.cnf'
alias tmux-sm='~/wiki/tmux/svm-tmux-env.sh'
alias tmux-sm-join='tmux a -t sm'

alias giti='cat ~milen/wiki/git/workflow/fix-issue.txt'
alias gitf='cat ~milen/wiki/git/workflow/new-feature.txt'
alias gitr='cat ~milen/wiki/git/workflow/new-release.txt'
alias g='git status; git log --all --decorate --oneline --graph -15'

alias restart-docky='killall docky 1>&2 2>/dev/null; nohup docky 1>&2 2>/dev/null &'

# Salon Manager project
alias wsm='cd /srv/http/dev/salon-manager/; vi'
alias cd-sm='cd /srv/http/dev/salon-manager/'
alias sm-bl='vi /srv/http/dev/salon-manager/backlog'

# Business Analysis project
alias wba='cd /srv/http/dev/ba/; vi'
alias cd-ba='cd /srv/http/dev/ba/'

# Wordpress GeneratePress
alias cd-gp='cd /srv/http/dev/wp/smgp1/'


alias cd-gr='cd /srv/http/dev/grafik/'

# Working with VirtualBox
alias startdb='VBoxManage startvm "FreeBSD" --type headless'
alias stopdb='VBoxManage controlvm "FreeBSD" poweroff'
alias startvms='VBoxManage startvm "FreeBSD" --type headless'
alias stopvms='VBoxManage controlvm "FreeBSD" poweroff'
alias vms='VBoxManage list runningvms'
alias allvms='VBoxManage list vms'

# Some command line tools
# Get external IP address:
alias extip='host `curl -s inet-ip.info`'

# Change the directory using fuzzy finder
alias cdf='cd `dirname $(fzf)`'

# Edit file using fuzzy finder
alias vif='vi $(fzf)'

# Edit all files with str in name
alias vifa='find . -type f -name "*${@}" | xargs vi'

# Edit all files with str in the content
alias vil='vi `ag -l $@`'
#alias vil='vi $(ag -l$@)'

# Vimdiff of two files selected with the help of fzf
alias vimdifff='vimdiff $(fzf) $(fzf)'

#alias ls='lsd '

alias ssha='eval $(ssh-agent) && ssh-add $@'

alias cdsm='cd /srv/http/dev/grafik && git status'
alias cddb='cd ~/projects/salon-manager/db'
alias cdsql='cd ~/projects/salon-manager/db/sql'
alias cdspl='cd ~/projects/salon-manager/db/spl'
alias cddoc='cd ~/projects/salon-manager/doc'
alias v='/usr/bin/vi $@ '

# Git aliases
git config --global alias.tags "!git log --tags --pretty='%h %d %s' --decorate=full"
git config --global alias.tagsonly "!git log --no-walk --tags --pretty='%h %d %s' --decorate=full"
