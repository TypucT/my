# BSPWM Window Manager


## Shortcuts

|Shortcut|Action|
|:----|:----|
|MOD+Enter|Open window with terminal emulator|
|MOD+h|Focus on the window to the left|
|MOD+l|Focus on the window to the right|
|MOD+;|Focus on the window to the right (used with windows hosts)|
|MOD+j|Focus on the window to the down|
|MOD+k|Focus on the window to the up|
|MOD+H|Swap the current window with the window to the left|
|MOD+L|Swap the current window with the window to the right|
|MOD+J|Swap the current window with the window to the down|
|MOD+K|Swap the current window with the window to the up|

