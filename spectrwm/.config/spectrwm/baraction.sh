#!/bin/bash
# baraction.sh script for spectrwm status bar

SLEEP_SEC=5  # set bar_delay = 5 in /etc/spectrwm.conf
COUNT=0
#loops forever outputting a line every SLEEP_SEC secs
while :; do
	let COUNT=$COUNT+1
        BAT=$(~/scripts/my/spectrwm-battery.sh)
        MEM=$(~/scripts/my/spectrwm-mem.sh)
        VPN=$(~/scripts/my/spectrwm-vpn.sh)
        WIFI=$(~/scripts/my/spectrwm-wifi.sh)
        # DATE=$(date "+%a %d.%m.%g %H:%M")
        echo -e "  ${VPN}  [ ${MEM} ]  [ ${BAT} ]  [ ${WIFI} ]"
        sleep $SLEEP_SEC
done
