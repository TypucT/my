" ##########################
"  GENRAL SETTINGS
" ##########################

let mapleader = ','		" Set the <Leader> key to comma instead of the default backslash
set nocompatible		" Use latest Vim options and not pure vi
set directory=$HOME/temp.tmp/vim-swapfiles//	" Location of VIM swap files
set clipboard^=unnamed,unnamedplus	" Use the system clipboard, helps yanking between vim instances
set backspace=indent,eol,start " Backspace will delete more simbols than inserted
set fileformats=unix,dos
set autowriteall		" Save buffer before switching to another.

" ---------- SCREEN SETTINGS ------------
" set rulerformat=%(%r\ [%n]\ %l:%c/%L\ %p%%%)     " The simplest status line I like
set rulerformat=%18([%n]\ %l:%c/%L%=\ %r%)     " The simplest status line I like
" set noruler

set tabstop=4
set softtabstop=4       " When in insert mode
set shiftwidth=4        " for fine tunned identation by pressing Shift-> when in normal mode
set expandtab           " fill with space's when pressing the tab key
set smarttab
set smartindent
set autoindent
set scrolloff=3

set number              " Show line numbers
set relativenumber      " Show relative line numbers
set hlsearch			" Highlight of the searched text
set incsearch           " Highlight the found searches while still typing the search pattern
set belloff=all         " Disable all audio bells
set noerrorbells		" No bells and warnings
set visualbell t_vb=	" No bells and warnings
set signcolumn=yes      " sets extracolumn on the left for errors and other messages. '=auto'
set colorcolumn=81      " transparent column to show the 81's simbol in the line
" set cursorline          " highlight the line I am on
set matchpairs+=<:>		" Add < and > to the matching brakets functionality along with (), {}, []

" autocompletion options
" simple autocompletion for the words in the file is enabled by Ctrl-n
" autocompletion for entire line Ctrl-x Ctrl-l
set complete=.,w,b,u	" Autocompletion order: current buf, windows, open bufs, not closed bufs
" automatically enable PHP autocompletion for every PHP file.
" use it by start typing function name and pressing Ctrl-x Ctrl-o
autocmd FileType php set omnifunc=phpcomplete#CompletePHP
autocmd FileType css set omnifunc=csscomplete#CompleteCSS
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
" remove the information messsage for the autocmpletion menu at the status bar
set shortmess+=c
set completeopt=menuone,longest     " see :help completeopt for info

" set guifont=Monospace\ Regular\ 18
" set guifont=Source\ Code\ Pro\ Medium\ 21
set guifont=SaurceCodePro\ Nerd\ Font\ Mono\ Medium\ 21
set linespace=5			" Settings for gvim - font and space between lines
syntax on



" #########################
" KEY MAPPINGS
" #########################

" -------
" Editing
" -------
" Map the ESC key when in insert mode
inoremap jj <ESC>

" Map j and k to move the line down and up
" inoremap <C-j> <Esc>:m .+1<CR>==gi
" inoremap <C-k> <Esc>:m .-2<CR>==gi
vnoremap _ :m '>+1<CR>gv=gv
vnoremap - :m '<-2<CR>gv=gv
nnoremap _ :m .+1<CR>==
nnoremap - :m .-2<CR>==

" Navigation in insert mode
inoremap <C-h>  <Left>
inoremap <C-l>  <Right>
inoremap <C-j>  <Down>
inoremap <C-k>  <Up>



" -------
" Macros
" -------
" nnoremap <Leader>q @q

" ##########################
"  VIM WINDOWS
" ##########################

" Ensure that horizontal split wil always be below
set splitbelow

" Ensure that vertical split will alwasy be on the right
set splitright

" Navigate between the windows on the screen
nnoremap <C-J> <C-W><C-J>
nnoremap <C-H> <C-W><C-H>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>



" Copy to the system register
nnoremap <Leader>cc "+yy

" Paste from the system register
nnoremap <Leader>cp "+p

" Edit .vimrc in new tab
nnoremap <Leader>ev :tabedit ~/.vimrc<cr>

" apply the .vimrc configuration
nnoremap <Leader>sv :so ~/.vimrc<cr>

" highlight removal
nnoremap <Leader><space> :nohlsearch<cr>

" Pressing Enter will add new line below the current line
nnoremap <CR> m`o<Esc>``

" Map Backspace to switching between last two files
nnoremap <bs> <c-^>

" Mapping for navigating via buffers
nnoremap <Leader>< :bp<CR>
nnoremap <Leader>> :bn<CR>

" When in terminal mode, escape the terminal with ESC as in normal mode
tnoremap <Esc> <C-\><C-n>

"--------------Resizing-------------------"
" TODO
" Increase the height of the current window
nnoremap <silent> <Leader>4 :resize +2 <CR>
" Decrease the height of the current window
nnoremap <silent> <Leader>3 :resize -2 <CR>
" Increase the width of the current window
nnoremap <silent> <Leader>2 :vertical resize +2 <CR>
" Decrease the width of the current window
nnoremap <silent> <Leader>1 :vertical resize -2 <CR>
" To increase a window to its maximum height, use Ctrl-w _
" To increase a window to its maximum width, use Ctrl-w |

" Toggle Tagbar shortcut
nnoremap <F8> :TagbarToggle<CR>

" Shortcut for NERDTree
nnoremap <space>s :NERDTreeToggle<cr>
nnoremap <space>l :NERDTreeFind<cr>

" CtrlP shortcut
nnoremap <Leader>pp :CtrlP<cr>
" :CtrlPBufTag<cr>
nnoremap <Leader>pb :CtrlPBuffer<cr>
nnoremap <Leader>pr :CtrlPMRUFiles<cr>

"--------------CtrlSF-------------------"
" Prompt for search
nmap     <Leader>ff <Plug>CtrlSFPrompt
" Search for the viusally selected word
vmap     <Leader>ff <Plug>CtrlSFVwordPath
" Search for the word under the cursor
nmap     <Leader>fw <Plug>CtrlSFCwordPath
" Search for the last search pattern
nmap     <Leader>fs <Plug>CtrlSFPwordPath
nnoremap <Leader>fo :CtrlSFOpen<CR>
nnoremap <Leader>ft :CtrlSFToggle<CR>
inoremap <Leader>ft <Esc>:CtrlSFToggle<CR>



"--------------SQL-------------------"
nnoremap <Leader>rss     :!  mysql --defaults-file=~/.config/mysql/my.cnf -t < %<cr>
nnoremap <Leader>mh     :!  pandoc -s -f markdown -t html --pdf-engine=xelatex -V mainfont=Arial -o %:r.html %<cr>
nnoremap <Leader>mp     :!  pandoc -s -f markdown -t latex -V fontenc=T2A -o %:r.pdf %<cr>



"--------------ASCIIDOC-------------------"
" HTML Help format in a directory
nnoremap <silent> <Leader>ahb     :!  a2x -L --icons -d book -a lang=bg -f htmlhelp  % <ENTER><ENTER>
nnoremap <silent> <Leader>ahe     :! a2x -L --icons -d book -f htmlhelp  % <ENTER><ENTER>
" Simple HTML
nnoremap <silent> <Leader>ab     :! a2x -L --icons -a lang=bg -f xhtml  % <ENTER><ENTER>
nnoremap <silent> <Leader>ae     :! a2x -L --icons -f xhtml  % <ENTER><ENTER>
"nnoremap <silent> <Leader>ab     :!  asciidoc -n -a lang=bg -b html5 % <ENTER>
"nnoremap <silent> <Leader>ae     :!  asciidoc -n -b html5 % <ENTER>


"--------------Tex-------------------"
nnoremap <Leader>mk :!make<CR>

"--------------Ack-------------------"
" Use ag (silver searcher) if available or, if not - ack
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif






" Plugin Manager
call plug#begin('~/.vim/plugged')

" Plug 'mhinz/vim-startify'

Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'ryanoasis/vim-devicons'

" if $VIMLINE
" Plug 'vim-airline/vim-airline'
" Plug 'vim-airline/vim-airline-themes'
" endif
Plug 'tpope/vim-fugitive'

Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'     " For repeating vim-surround commands by using the dot command
" Plug 'machakann/vim-sandwich'

Plug 'ctrlpvim/ctrlp.vim'               " CtrlP plugin for fuzzy search

Plug 'ap/vim-css-color'

Plug 'tpope/vim-commentary'             " Ex.: gc, gcc, 5gcc, gcap, :g/TODO/Commentary, :7,17Commenatry

" Plug 'majutsushi/tagbar'                " Pannel on the right with the tags
Plug 'preservim/tagbar'                 " The new tagbar pannel

Plug 'petdance/ack2'                    " needed for CtrSF
Plug 'dyng/ctrlsf.vim'                  " Searching feature - CtrSF

" Plug 'morhetz/gruvbox'
Plug 'morhetz/gruvbox'
Plug 'NLKNguyen/papercolor-theme'

" Add plugins to &runtimepath
call plug#end()

let g:tagbar_width=40
let g:tagbar_show_tag_linenumbers = 1

" if $VIMLINE
" let g:airline_powerline_fonts = 1
" let g:airline#extensions#tabline#enabled = 1
" let g:airline#extensions#tabline#left_sep = ' '
" let g:airline#extensions#tabline#left_alt_sep = '|'
" let g:airline_theme='distinguished'
" endif

let g:NERDTreeIgnore = ['^node_modules$']

let g:ctrlp_match_window = 'bottom,order:btt,min:1,max:30,results:30'
let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']

let g:ctrlsf_default_root = 'project'
let g:ctrlsf_position = 'left'
let g:ctrlsf_winsize = '50%'


" Enable highlighting of code inside markdown.
" Add the <?php tag at the beginning of code.
let g:markdown_fenced_languages = ['php', 'python', 'sh']

" Open NerdTree on the right-hand side
let g:NERDTreeWinPos = "right"



" The TERM shell variable must show that the terminal supports 256 colors.
" a) screen-256colors
" b) xterm-256color
set t_Co=256			" VIM support for 256 colors.
"colorscheme PaperColor
colorscheme gruvbox
set background=dark

iabbrev php. <?php



" #############
" # Functions
" #############
function! MarkWindowSwap()
    let g:markedWinNum = winnr()
endfunction

function! DoWindowSwap()
    "Mark destination
    let curNum = winnr()
    let curBuf = bufnr( "%" )
    exe g:markedWinNum . "wincmd w"
    "Switch to source and shuffle dest->source
    let markedBuf = bufnr( "%" )
    "Hide and open so that we aren't prompted and keep history
    exe 'hide buf' curBuf
    "Switch to dest and shuffle source->dest
    exe curNum . "wincmd w"
    "Hide and open so that we aren't prompted and keep history
    exe 'hide buf' markedBuf
endfunction

" function! TrimWhiteSpace()
"     let l:save = winsaveview()
"     keeppattern %s/\s\+$//e
"     call winrestview(l:save)
" endfun

" augroup MYGRP
"     autocmd!
"     autocmd BufWritePre * :call TrimWhiteSpace()
" augroup END


nnoremap <silent> <leader>yw :call MarkWindowSwap()<CR>
nnoremap <silent> <leader>pw :call DoWindowSwap()<CR>




" set rulerformat=%55(%{system('git\ branch\ --show-current')}\ [%n]\ %l,%c\ %p%%%)
"set rulerformat=%35([%{system('basename\ $PWD')}][%{system('git\ branch\ --show-current')}]\ [%n]\ %l,%c\ %p%%%)
