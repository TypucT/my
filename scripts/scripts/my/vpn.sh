
# Prints the number of VPN connections
NUM_VPNs=$(ip a | grep tun | grep -c inet)

for (( i=0; i < $NUM_VPNs; i++ ))
do
    echo -n " "
done
echo
