#!/usr/bin/env sh

# TMUX colors can be added later
free -h -t | awk '/^Mem:/ {printf "%s|%s\n", $3, $4 }'
