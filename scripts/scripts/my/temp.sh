#echo $(acpi -t | cut -d" " -f4|sort -r|head -1)°C


acpi -t  | awk 'BEGIN {total = 0; cnt = 0; max = 0} {if( $4> 0.0) {total += $4; if ($4>max) {max = $4}; cnt++}} END { if(total/cnt==max) { printf "%3.1f\n", max } else { printf "%3.1f/%3.1f\n", total/cnt,max} }' 
