L=`acpi -b | sed -n -E "s/.* ([^0][0-9]*%).*/\1/p"`
#L=`acpi -b | sed -n "s/.*\s\([1-9]\+%\).*/\1/p"`
D=`acpi -a | sed -n "s/^.\+\(off\|on\)-line/\1/p"`
if [[ "${D}" == "off" ]] ; then
    L="${L}-"
elif [[ "${D}" == "on" ]]; then
    L="${L}+"
else
    L="${L}"
fi
echo ${L}

