# My dotfiles and other configurations.


## Requirements

1. The _stow_ command must be installed on the system. For example, on arch based systems run:

```sh
sudo pacman -S stow --needed --noconfirm
```

## tmux


## vim


### Install vim(gvim)

```sh
sudo pacman -S gvim --needed --noconfirm
```

### Directory for vim swap files

Create directory for vim swap files.

```sh
mkdir ~/temp.tmp/vim-swapfiles
```

### VIM Package Manager

Install the package manager for vim:

```sh
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

or

Install the package manager for nvim:

```sh
curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

After the package manager is installed run vim with the following command to install the packages from the .vimrc fie.

```sh
vim +PlugInstall
```



#### Keyboard Language Layout Indicator

Install the keyboard language layout indicator for (n)vim status line:

```sh
echo "===> xkb-switch for VIM --> installation START"

sudo pacman -S cmake --noconfirm
mkdir -p ~/github
cd ~/github
git clone https://github.com/ierton/xkb-switch
cd xkb-switch
mkdir build && cd build
cmake ..
make
sudo make install

echo "===> xkb-switch for VIM --> installation END"
```

Keyboard language layout indicator can be enabled in vim status line by the following configuration:

```sh
" enable/disable vim-xkblayout extension
let g:airline#extensions#xkblayout#enabled = 1

" redefine keyboard layout short codes to shown in status
" 'BG' instead of system 'Bulgarian-Phonetic',
" 'RU' instead of system 'Russian-Phonetic',
" 'EN' instead of system 'ABC'.
let g:airline#extensions#xkblayout#short_codes = {'Russian-Phonetic': 'RU', 'ABC': 'EN', 'Bulgarian-Phonetic':'BG'}

" define path to the backend switcher library
let g:XkbSwitchLib = '/usr/local/lib/libxkbswitch.so'
```

